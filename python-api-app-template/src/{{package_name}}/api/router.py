from typing import Any

from fastapi import APIRouter, Response, status


router = APIRouter()

@router.get("/v1/hello")
async def hello_world() -> dict[str, str]:
    from ..config import settings
    return {
        "text": "hello world",
        "environment": settings.ENVIRONMENT
    }

