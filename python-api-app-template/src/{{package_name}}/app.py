from contextlib import asynccontextmanager
import asyncio
from typing import Any
from fastapi import FastAPI
from fastapi.responses import RedirectResponse
from starlette.middleware.cors import CORSMiddleware

from .config import app_configs, settings
from .api.router import router as api_router
from .metrics_instrumentation import (emit_system_metric, configure_metric_instrumentator)

@asynccontextmanager
async def lifespan(app: FastAPI):
    if not settings.ENVIRONMENT.is_testing:
        asyncio.create_task(emit_system_metric())
    yield

app = FastAPI(**app_configs, lifespan=lifespan)

app.add_middleware(
    CORSMiddleware,
    allow_origins = settings.CORS_ORIGINS,
    allow_origin_regex = settings.CORS_ORIGIN_REGEX,
    allow_credentials = True,
    allow_methods = ("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"),
    allow_headers = settings.CORS_HEADERS,
)

configure_metric_instrumentator(app)

@app.get("/", include_in_schema=False)
async def root_redirect():
    response = RedirectResponse(url='/docs')
    return response

@app.get("/healthz", include_in_schema=False)
async def healthcheck() -> dict[str, str]:
    return {"status": "ok"}

app.include_router(api_router, prefix="/api", tags=["api"])

