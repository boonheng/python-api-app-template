from typing import Any, Dict, Optional

from fastapi import HTTPException, status

class AppException(HTTPException):
    STATUS_CODE = status.HTTP_500_INTERNAL_SERVER_ERROR
    DETAIL = "Server error"

    def __init__(self, **kwargs: dict[str, Any]) -> None:
        super().__init__(status_code=self.STATUS_CODE, detail=self.DETAIL, **kwargs)

class PermissionDenied(AppException):
    STATUS_CODE = status.HTTP_403_FORBIDDEN
    DETAIL = "Permission denied"

class BadRequest(AppException):
    STATUS_CODE = status.HTTP_400_BAD_REQUEST
    DETAIL = "Bad Request"

class NotFound(AppException):
    STATUS_CODE = status.HTTP_404_NOT_FOUND

class NotAuthenticated(AppException):
    STATUS_CODE = status.HTTP_401_UNAUTHORIZED
    DETAIL = "User is not authenticated"

    def __init__(self) -> None:
        super().__init__(headers={"WWW-Authenticate": "Bearer"})