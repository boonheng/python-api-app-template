from enum import Enum

class Environment(str, Enum):
    DEV = "dev"
    STAGING = "staging"
    PRODUCTION = "production"
    TESTING = "testing"

    @property
    def is_testing(self) -> bool:
        return self == self.TESTING
    
    @property
    def is_deployed(self) -> bool:
        return self in (self.STAGING, self.PRODUCTION)
