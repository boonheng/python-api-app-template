import asyncio
import logging
from logging import LogRecord
import psutil
from fastapi import FastAPI
from prometheus_client import Gauge
from prometheus_fastapi_instrumentator import PrometheusFastApiInstrumentator

"""additional metics"""

memory_metric = Gauge(
    "memory_usage_bytes", "Memory usage in bytes.",
)

cpu_metric = Gauge(
    "cpu_usage_percent", "CPU usage percent.",
)

async def emit_system_metric():
    while True:
        memory_metric.set(psutil.virtual_memory()[2])
        cpu_metric.set(psutil.cpu_percent())
        await asyncio.sleep(1)


class EndpointMetricsFilter(logging.Filter):
    # filter off metrics endpoint from access log
    def filter(self, record: LogRecord) -> bool:
        return record.getMessage().find("GET /metrics") == -1


def configure_metric_instrumentator(app: FastAPI):
    PrometheusFastApiInstrumentator(
        excluded_handlers=["/metrics", "/healthz", "/openapi.json"],
    ).instrument(
        app,
    ).expose(
        app, 
        endpoint="/metrics",
        include_in_schema=False, should_gzip=False
    )

    logging.getLogger("uvicorn.access").addFilter(EndpointMetricsFilter())