
# Python API Application Template

This project template codifies my best practices for a typical Python API application based on `FastAPI`. It set a foundation for python code organization, testing, documentation, and day 2 operation. In summary, it take care:

- Wiring for FAST API app including CORS, etc 
- Observability
- CI/CD pipeline


It is meants to help new projects on Python API application get started quickly, and abstracting enough concerns to enable developers to focus on writing code. 

Template is based on [Copier](https://copier.readthedocs.io/en/latest) and thus, it is required to use this template. Copier is an open source tool that hydrates projects from templates and it natively supports updating projects as the original template matures. It pretty neat!

This template works best with Copier v8.0 and above. 

## Getting Started

Choose where you would like to create your new python API appkication project, and 

```sh
# download and use it as a cli app
pipx install copier

# this is how to use the template
>$ copier gl:boonheng/python-api-app-template <path/to/new/project/destination>
```

## Contributing to the Template

Pend writeup on contribution guide